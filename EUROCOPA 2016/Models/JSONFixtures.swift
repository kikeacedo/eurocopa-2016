//
//  Fixtures.swift
//  UEFA EURO 2016
//
//  Created by Quique Acedo on 30/5/16.
//  Copyright © 2016 Quique Accedo. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class JSONFixtures:Mappable{
    
    var count : Int?
    var fixtures:[JSONMatch]?
    
    required init?(_ map: Map){
    }
    
    func mapping(map: Map) {
        count <- map["count"]
        fixtures <- map["fixtures"]
    }//mapping
    
    func printMatches(){
        for match in fixtures!{
            print("\(match.homeTeamName) - \(match.awayTeamName) on \(match.printResult())")
        }//for
    }//printMatches
    
    func getMatches() -> List<Match>{
        let list = List<Match>()
        var count = 0
        for jsonMatch in fixtures!{
            let match:Match = Match(jsonMatch:jsonMatch, id: count)
            list.append(match)
            count = count + 1
        }//for
        
        return list
    }//saveMatches
}
