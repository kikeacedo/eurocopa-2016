//
//  FixtureCell.swift
//  EUROCOPA 2016
//
//  Created by Quique Acedo on 31/5/16.
//  Copyright © 2016 Quique Acedo. All rights reserved.
//

import Foundation
import UIKit

class FixtureCell:UITableViewCell{
    
    @IBOutlet weak var homeTeam: UILabel!
    @IBOutlet weak var awayTeam: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var resultImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
