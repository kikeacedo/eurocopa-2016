//
//  DataRepository.swift
//  UEFA EURO 2016
//
//  Created by Quique Acedo on 31/5/16.
//  Copyright © 2016 Quique Accedo. All rights reserved.
//

import Foundation
import RealmSwift

class MatchRepository:DataRepositoryProtocol{
    
    init(){}
    
    func insertMatch(match: Match) {
        let realm = try! Realm()
        do {
            try realm.write {
                realm.add(match)
                print("Inserted: \(match.homeTeam) - \(match.awayTeam)")
            }
        } catch {
            print("Error writing to Realm")
        }
    }//insertMatch
    
    func insertMatches(matches: [Match]) {
        let realm = try! Realm()
        for match in matches{
            do {
                try realm.write {
                    realm.add(match)
                    print("Inserted: \(match.homeTeam) - \(match.awayTeam)")
                }
            } catch {
                print("Error writing to Realm")
            }
        }
        
    }//insertMatches
    
    func getMatch(homeTeam:String, awayTeam:String) -> Match {
        let realm = try! Realm()
        let match = realm.objects(Match).filter("homeTeam = '\(homeTeam)' AND awayTeam= '\(awayTeam)'")
        
        return match[0]
    }
    
    func getNumMatches() -> Int{
        let realm = try! Realm()
        let matches = realm.objects(Match)
        return matches.count
    }
    
    func updateMatch(match: Match) {
        let realm = try! Realm()
        do {
            try realm.write {
                realm.add(match, update: true)
//                print("Updated: \(match.homeTeam) - \(match.awayTeam)")
            }
        } catch {
            print("Error writing to Realm")
        }
    }
    
    func updateMatchFromApi(match: Match) {
        let realm = try! Realm()
        do {
            try realm.write {
                let matchToUpdate = realm.objectForPrimaryKey(Match.self, key: match.id)
            
                matchToUpdate?.awayTeam = match.awayTeam
                matchToUpdate?.homeTeam = match.homeTeam
                matchToUpdate?.date = match.date
                matchToUpdate?.goalsAwayTeam = match.goalsAwayTeam
                matchToUpdate?.goalsHomeTeam = match.goalsHomeTeam
                
//                print("Updated from API: \(match.homeTeam) - \(match.awayTeam)")
            }
        } catch {
            print("Error writing to Realm")
        }
    }
    
    func deleteMatches() {
        let realm = try! Realm()

        try! realm.write {
            realm.deleteAll()
        }
    }
}//MatchRepository