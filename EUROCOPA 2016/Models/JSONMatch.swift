//
//  Match.swift
//  UEFA EURO 2016
//
//  Created by Quique Acedo on 30/5/16.
//  Copyright © 2016 Quique Accedo. All rights reserved.
//

import Foundation
import ObjectMapper

class JSONMatch:Mappable{
    
    var status : String?  //"TIMED",
    var matchday : Int? //1,
    var homeTeamName : String? //"France",
    var awayTeamName : String? //"Romania",
    var result : JSONResult?
    var date:String? //"2016-06-10T19:00:00Z",
    
    
    required init?(_ map: Map){
        
    }
    
    func mapping(map: Map) {
        date <- map["date"]
        status <- map["status"]
        matchday <- map["matchday"]
        homeTeamName <- map["homeTeamName"]
        awayTeamName <- map["awayTeamName"]
        result <- map["result"]
    }
    
    func printResult(){
        print("\(result?.goalsHomeTeam) - \(result?.goalsAwayTeam) on \(matchday) (\(date))")
    }
    
    func getDate() -> NSDate{
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"//2016-06-10T19:00:00Z
        return dateFormatter.dateFromString(date!)!
    }//getDate
    
    func getGoalsAwayTeam()->Int{
        return (result?.goalsAwayTeam)!
    }
    
    func getGoalsHomeTeam()->Int{
        return (result?.goalsHomeTeam)!
    }

}//class

class JSONResult:Mappable{
    var goalsHomeTeam = 0
    var goalsAwayTeam = 0
    
    required init?(_ map: Map){
        
    }
    
    func mapping(map: Map) {
        goalsAwayTeam <- map["goalsAwayTeam"]
        goalsHomeTeam <- map["goalsHomeTeam"]
    }
    
}//Result
