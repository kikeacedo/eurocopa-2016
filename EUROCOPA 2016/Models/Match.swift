//
//  Match.swift
//  UEFA EURO 2016
//
//  Created by Quique Acedo on 31/5/16.
//  Copyright © 2016 Quique Accedo. All rights reserved.
//

import Foundation
import RealmSwift

class Match:Object{
    
    dynamic var id = 0
    dynamic var homeTeam : String? = nil
    dynamic var awayTeam : String? = nil
    dynamic var date : NSDate? = nil
    dynamic var goalsHomeTeam = 0
    dynamic var goalsAwayTeam = 0
    dynamic var goalsAwayPrediction = 0
    dynamic var goalsHomePrediction = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(jsonMatch:JSONMatch, id:Int) {
        self.init()
        self.id = id
        homeTeam = jsonMatch.homeTeamName
        awayTeam = jsonMatch.awayTeamName
        date = jsonMatch.getDate()
        goalsHomeTeam = jsonMatch.getGoalsAwayTeam()
        goalsAwayTeam = jsonMatch.getGoalsAwayTeam()
    }
    
    convenience init(id:Int, homeTeam:String, awayTeam:String, homeGoals:Int, awayGoals:Int, homeGoalsPrediction:Int, awayGoalsPrediction:Int){
        self.init()
        self.id = id
        self.awayTeam = awayTeam
        self.homeTeam = homeTeam
        self.goalsHomeTeam = homeGoals
        self.goalsAwayTeam = awayGoals
        self.goalsHomePrediction = homeGoalsPrediction
        self.goalsAwayPrediction = awayGoalsPrediction
        
    }
    
    convenience init(match:Match,homeGoalsPrediction:Int, awayGoalsPrediction:Int ){
        self.init()
        self.id = match.id
        self.awayTeam =  match.awayTeam
        self.homeTeam =  match.homeTeam
        self.goalsHomeTeam =  match.goalsHomeTeam
        self.goalsAwayTeam =  match.goalsAwayTeam
        self.date = match.date
        self.goalsHomePrediction = homeGoalsPrediction
        self.goalsAwayPrediction = awayGoalsPrediction
    }
    
    func getHomeFlag()->UIImage{
        //        return UIImage(named:"Flags/\(homeTeam)-Flag.icns")!
        return UIImage(named:"Flags/France-Flag.icns")!
    }//getHomeFlag
    
    func hasStarted()-> Bool{
        let now:NSDate! = NSDate()
        return self.date?.earlierDate(now) != self.date // (!= to test the results because first match is 10/6/16)
    }//hasStarted
    
    func rigthHomeGoals() -> Bool{
        return goalsHomePrediction == goalsHomeTeam
    }
    
    func rigthAwayGoals() -> Bool{
        return goalsAwayPrediction == goalsAwayTeam
    }
    
    func erros() -> Int{
        var num_erros = 0
        
        if(!rigthHomeGoals()){
            num_erros = num_erros + 1
        }
        
        if(!rigthAwayGoals()){
            num_erros = num_erros + 1
        }
        
        return num_erros
    }
}