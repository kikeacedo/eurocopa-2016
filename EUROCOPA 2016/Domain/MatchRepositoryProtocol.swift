//
//  MatchRepositoryInterface.swift
//  UEFA EURO 2016
//
//  Created by Quique Acedo on 31/5/16.
//  Copyright © 2016 Quique Accedo. All rights reserved.
//

import Foundation

protocol DataRepositoryProtocol{
    
    func insertMatch(match:Match)

    func updateMatch(match:Match)

    func updateMatchFromApi(match:Match)

    func insertMatches(matches:[Match])
    
    func getMatch(homeTeam:String, awayTeam:String)->Match

    func getNumMatches() -> Int
    
    func deleteMatches()
}