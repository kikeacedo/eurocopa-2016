//
//  MainPresenter.swift
//  Eurocopa 2016
//
//  Created by Quique Acedo on 7/6/16.
//  Copyright © 2016 Quique Acedo. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import RealmSwift

class MainPresenter:MainProtocol{
    
    init(){}
    
    func getApiData(result:(resOK:Bool)->()) {
        // Do any additional setup after loading the view, typically from a nib.
        let URL =  NSURL(string: "http://api.football-data.org/v1/soccerseasons/424/fixtures")
        let mutableURLRequest = NSMutableURLRequest(URL: URL!)
        mutableURLRequest.setValue("157cce660dae4581971db2b75733ebd0", forHTTPHeaderField: "X-Auth-Token")
        
        let manager = Alamofire.Manager.sharedInstance
        let request = manager.request(mutableURLRequest)
        
        request.responseObject{(response:Response<JSONFixtures, NSError>) in
            
            let fixtures = response.result.value
            
            if(response.result.isSuccess){
                let repository = MatchRepository()
                
                if repository.getNumMatches() == 0 {
                    let list = fixtures?.getMatches()
                    for match in list!{
                        repository.insertMatch(match)
                        result(resOK:true)
                    }
                }else{
                    let list = fixtures?.getMatches()
                    for match in list!{
                        repository.updateMatchFromApi(Match(match:match, homeGoalsPrediction:match.goalsHomePrediction, awayGoalsPrediction:match.goalsAwayPrediction))
                        result(resOK:true)
                    }//for
                    
                }//if-else
            }else{
                result(resOK: false)
            }
            
        }//responseObject
        
    }//getApiData
}//MainPresenter