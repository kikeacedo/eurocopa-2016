//
//  File.swift
//  Eurocopa 2016
//
//  Created by Quique Acedo on 7/6/16.
//  Copyright © 2016 Quique Acedo. All rights reserved.
//

import Foundation


protocol MainProtocol{
    
    func getApiData(result:(resOK:Bool)->())
}