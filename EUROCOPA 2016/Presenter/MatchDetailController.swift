//
//  MatchDetailController.swift
//  Eurocopa 2016
//
//  Created by Quique  on 01/06/16.
//  Copyright © 2016 Quique Acedo. All rights reserved.
//

import UIKit
import RealmSwift


class MatchDetailController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var time: UILabel!
    
    @IBOutlet weak var homeTeam: UILabel!
    @IBOutlet weak var awayTeam: UILabel!
    
    @IBOutlet weak var homeGoals: UILabel!
    @IBOutlet weak var awayGoals: UILabel!
    
    @IBOutlet weak var homePredictionGoals: UITextField!
    @IBOutlet weak var awayPredictionGoals: UITextField!
    
    @IBOutlet weak var saveButton: UIButton!
    
    let realm = try! Realm()
    
    var match:Match!
    
    var homeTeamDetail:String!
    var awayTeamDetail:String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let repository = MatchRepository()
        
        match = repository.getMatch(homeTeamDetail, awayTeam: awayTeamDetail)
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "EEEE dd 'de' MMMM, yyyy"//2016-06-10T19:00:00Z
        
        let timeFormatter = NSDateFormatter()
        timeFormatter.dateFormat = "HH:mm"//2016-06-10T19:00:00Z
        
        date.text = dateFormatter.stringFromDate(match.date!)
        time.text = timeFormatter.stringFromDate(match.date!)
        homeTeam.text = match.homeTeam
        awayTeam.text = match.awayTeam
        homeGoals.text = String(match.goalsHomeTeam)
        awayGoals.text = String(match.goalsAwayTeam)
        homePredictionGoals.text = String(match.goalsHomePrediction)
        awayPredictionGoals.text = String(match.goalsAwayPrediction)
        
        if match.hasStarted(){
            saveButton.hidden = true
            homePredictionGoals.enabled = false
            awayPredictionGoals.enabled = false
            
            if match.rigthHomeGoals(){
                homePredictionGoals.backgroundColor = UIColor.greenColor() // rgb(152,251,152)
            }else{
                homePredictionGoals.backgroundColor = UIColor.redColor() // rgb(152,251,152)
            }
            
            if match.rigthAwayGoals(){
                awayPredictionGoals.backgroundColor = UIColor.greenColor() // rgb(152,251,152)
            }else{
                awayPredictionGoals.backgroundColor = UIColor.redColor() // rgb(152,251,152)
            }
        }else{
            homeGoals.text = "-"
            awayGoals.text = "-"
        }
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?){
        view.endEditing(true)
        super.touchesBegan(touches, withEvent: event)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onClickSaveButton(sender: AnyObject) {
        var homePrediction:Int = 0
        var awayPrediction:Int = 0
        var errors:Bool = false
        
        if(homePredictionGoals.text != ""){
            homePrediction = Int(homePredictionGoals.text!)!
        }
        else{
            errors = true
        }
        
        if(awayPredictionGoals.text != ""){
            awayPrediction = Int(awayPredictionGoals.text!)!
        }else{
            errors =  true
        }
        
        if(!errors){
            let repository = MatchRepository()
            repository.updateMatch(Match(match:match, homeGoalsPrediction:homePrediction, awayGoalsPrediction:awayPrediction))
            
            let alertController = UIAlertController(title: "Saved result", message:
                "\(homePrediction) - \(awayPrediction) saved.\nGood luck!", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }else{
            let repository = MatchRepository()
            repository.updateMatch(Match(match:match, homeGoalsPrediction:homePrediction, awayGoalsPrediction:awayPrediction))
            
            let alertController = UIAlertController(title: "Incomplete fields", message:
                "Please complete all fields.", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Understood", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
