//
//  ViewController.swift
//  EUROCOPA 2016
//
//  Created by Quique Acedo on 31/5/16.
//  Copyright © 2016 Quique Acedo. All rights reserved.
//

import UIKit
import RealmSwift


class MainController: UITableViewController {
    
    let realm = try! Realm()
    var homeTeamDetail:String!
    var awayTeamDetail:String!
    
    let presenter = MainPresenter()
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return realm.objects(Match).count
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let items = realm.objects(Match)
        let cell = tableView.dequeueReusableCellWithIdentifier("MatchCell", forIndexPath: indexPath) as! FixtureCell
        let match:Match = items[indexPath.row]
        
        cell.homeTeam.text = match.homeTeam
        cell.awayTeam.text = match.awayTeam
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM"//2016-06-10T19:00:00Z
        
        cell.date!.text = dateFormatter.stringFromDate(match.date!)
        
        if(match.hasStarted()){
            switch match.erros() {
            case 0:
                cell.resultImage.image = UIImage(named: "ic_thumb_up")
                
            case 1:
                cell.resultImage.image = UIImage(named: "ic_thumbs_up_down")
                
            case 2:
                cell.resultImage.image = UIImage(named: "ic_thumb_down")
                
            default:
                cell.resultImage.hidden = true
            }
        }else{
            cell.resultImage.hidden = true
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        let indexPath = tableView.indexPathForSelectedRow!
        let currentCell = tableView.cellForRowAtIndexPath(indexPath)! as! FixtureCell
        
        homeTeamDetail = currentCell.homeTeam.text
        awayTeamDetail = currentCell.awayTeam.text
        
        performSegueWithIdentifier("MatchDetail", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?){
        
        if (segue.identifier == "MatchDetail") {
            
            // initialize new view controller and cast it as your view controller
            let viewController = segue.destinationViewController as! MatchDetailController
            // your new view controller should have property that will store passed value
            viewController.homeTeamDetail = homeTeamDetail
            viewController.awayTeamDetail = awayTeamDetail
        }
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        presenter.getApiData({(resultOk) in
            if(resultOk){
                self.reloadTable()
            }else{
                let alertController = UIAlertController(title: "Api failure", message:
                    "There have been a problem downloading data", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Understood", style: UIAlertActionStyle.Default,handler: nil))
                
                self.presentViewController(alertController, animated: true, completion: nil)
                
            }
        })
    }//viewDidLoad
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }//didReceiveMemoryWarning
    
    func reloadTable(){
        self.tableView.reloadData()
    }
}

